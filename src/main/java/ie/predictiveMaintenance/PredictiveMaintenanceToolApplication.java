package ie.predictiveMaintenance;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import ie.predictiveMaintenance.Controller.DataController;
import ie.predictiveMaintenance.Controller.ProcessController;
import ie.predictiveMaintenance.entities.Sensor;

@SpringBootApplication
public class PredictiveMaintenanceToolApplication implements CommandLineRunner {
	private static Logger logger = LoggerFactory.getLogger(PredictiveMaintenanceToolApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(PredictiveMaintenanceToolApplication.class, args);
	}

	@Autowired
	private DataController dataController;

	@Autowired
	private ProcessController processController;

	@Override
	public void run(String... args) throws Exception {

		while (true) {
			try {
				Thread.sleep(30000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			//List<Object> data = dataController.showSensor();// gets data
			List<Object> data = dataController.getAllSensors();

			if (data != null) {
				List<Sensor> updatedSensors = processController.processData(data);
				dataController.sendStatus(updatedSensors);
			}
			
			//dataController.getAllSensors();
		}
//		if(data != null) {
//			logger.info("PM main data " + data.get(0).toString());
//			List<Object> head = processController.getHead(data);
//			logger.info("PM main head " + head.get(0).toString());
//			dataController.sendHead(head);
//		}
		/**
		 * the following code deals with one sensor at a time
		 */
//		while (true) {
//			try {
//				Thread.sleep(30000);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//			Object dataById = dataController.getDataById();// gets data by topic
//
//			if (dataById != null) {
//				Sensor updatedSensor = processController.processDataById(dataById);
//				dataController.sendStatusById(updatedSensor);
//			}
//		}
	}

}
