package ie.predictiveMaintenance.Controller;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import ie.predictiveMaintenance.entities.Sensor;
import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class DataController {
	private static Logger logger = LoggerFactory.getLogger(DataController.class);

	// @GetMapping("/showSensor/")
	public List<Object> showSensor() {
		try {
			RestTemplate restTemplate = new RestTemplate();
			String URL = "http://localhost:8080/api/sensorData";
			String userName = "";
			String password = "";
			HttpHeaders headers = createHeaders(userName, password);

			ParameterizedTypeReference<List<Object>> myData = new ParameterizedTypeReference<List<Object>>() {
			};
			ResponseEntity<List<Object>> responseEntity = restTemplate.exchange(URL, HttpMethod.GET,
					new HttpEntity<>(headers), myData);
			List<Object> data = responseEntity.getBody();
			logger.info("showSensor: --------------------------------------------------");
			logger.info("data length: " + Integer.toString(data.size()));
			if (data.size() > 0) {
				logger.info(data.get(0).toString());
			}
//		        for(String reading : data) {
//		        	logger.info("reading: " + reading);
//		        }

			return data;
		} catch (HttpClientErrorException ex) { // all 4xx errors codes
			logger.info("dataController exception", ex.getMessage());
			return null;
		}
	}

	public Object getDataById() {
		String id = "sensor1";
		try {
			RestTemplate restTemplate = new RestTemplate();
			String URL = "http://localhost:8080/api/sensorData/" + id;
			String userName = "";
			String password = "";
			HttpHeaders headers = createHeaders(userName, password);

			ParameterizedTypeReference<Object> myData = new ParameterizedTypeReference<Object>() {
			};
			ResponseEntity<Object> responseEntity = restTemplate.exchange(URL, HttpMethod.GET,
					new HttpEntity<>(headers), myData);
			Object data = responseEntity.getBody();
			List<Object> theData = (List<Object>) data;
			logger.info("getDataById: --------------------------------------------------");
			// logger.info("data length: " + Integer.toString(data.size()));
			logger.info(theData.get(0).toString());
//	        if(data.size() > 0) {
//	        	for(int i = 0;i < data.size();i++) {
//	        		logger.info(data.get(i).toString());       		
//	        	}
//	        }
//	        for(String reading : data) {
//	        	logger.info("reading: " + reading);
//	        }

			return theData.get(0);
		} catch (HttpClientErrorException ex) { // all 4xx errors codes
			logger.info("dataController exception", ex.getMessage());
			return null;
		}
	}

	public List<Object> getAllSensors() {
		try {
			RestTemplate restTemplate = new RestTemplate();
			String URL = "http://localhost:8080/api/allSensors/";
			String userName = "";
			String password = "";
			HttpHeaders headers = createHeaders(userName, password);

			ParameterizedTypeReference<List<Object>> myData = new ParameterizedTypeReference<List<Object>>() {
			};
			ResponseEntity<List<Object>> responseEntity = restTemplate.exchange(URL, HttpMethod.GET,
					new HttpEntity<>(headers), myData);
			List<Object> data = responseEntity.getBody();
			logger.info("showSensor: --------------------------------------------------");
			logger.info("data length: " + Integer.toString(data.size()));
			if (data.size() > 0) {
				logger.info(data.get(0).toString());
			}
//		        for(String reading : data) {
//		        	logger.info("reading: " + reading);
//		        }

			return data;
		} catch (HttpClientErrorException ex) { // all 4xx errors codes
			logger.info("dataController getAllSensors exception", ex.getMessage());
			return null;
		}
	}
	private HttpHeaders createHeaders(String userName, String password) {

		return new HttpHeaders() {
			{
				String auth = userName + ":" + password;
				byte[] encodeStringIntoBytes = auth.getBytes(StandardCharsets.UTF_8);
				byte[] encodedAuth = Base64.encodeBase64(encodeStringIntoBytes);
				String authHeader = "Basic " + new String(encodedAuth);
				log.info("INFO...{}", authHeader);
				setContentType(MediaType.APPLICATION_JSON);
				// setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
				set(HttpHeaders.AUTHORIZATION, authHeader);
			}
		};
	}

	public void sendHead(List<Object> head) {
		for (Object topic : head) {
			try {
				RestTemplate restTemplate = new RestTemplate();
				String URL = "http://localhost:8080/api/head";
				String userName = "";
				String password = "";
				HttpHeaders headers = createHeaders(userName, password);

				ParameterizedTypeReference<Object> myData = new ParameterizedTypeReference<Object>() {
				};
				ResponseEntity<Object> responseEntity = restTemplate.exchange(URL, HttpMethod.POST,
						new HttpEntity<>(topic, headers), myData);
				/*
				 * maybe test here on the responseEntity??
				 * 
				 * do i return anything??
				 */

				// return data;
			} catch (HttpClientErrorException ex) { // all 4xx errors codes
				logger.info("exception", ex);
				// return null;
			}
		}
	}

	public void sendStatus(List<Sensor> sensors) {
		for (Object topic : sensors) {
			Sensor sensor = (Sensor) topic;
			logger.info("sendStatus topic " + sensor.getName() + " " + sensor.getData());
			Object sensorObject = sensor;
			try {
				RestTemplate restTemplate = new RestTemplate();
				String URL = "http://localhost:8080/api/status";
				String userName = "";
				String password = "";
				HttpHeaders headers = createHeaders(userName, password);

				ParameterizedTypeReference<Object> myData = new ParameterizedTypeReference<Object>() {
				};
				ResponseEntity<Object> responseEntity = restTemplate.exchange(URL, HttpMethod.POST,
						new HttpEntity<>(sensorObject, headers), myData);
				/*
				 * maybe test here on the responseEntity??
				 * 
				 * do i return anything??
				 */

				// return data;
			} catch (HttpClientErrorException ex) { // all 4xx errors codes
				logger.info("exception", ex);
				// return null;
			}

		}
	}

	public void sendStatusById(Sensor sensor) {
		// for(Object topic: sensors) {
		// Sensor sensor = (Sensor) topic;
		logger.info("topic " + sensor.getName());
		Object sensorObject = sensor;
		try {
			RestTemplate restTemplate = new RestTemplate();
			String URL = "http://localhost:8080/api/status";
			String userName = "";
			String password = "";
			HttpHeaders headers = createHeaders(userName, password);

			ParameterizedTypeReference<Object> myData = new ParameterizedTypeReference<Object>() {
			};
			ResponseEntity<Object> responseEntity = restTemplate.exchange(URL, HttpMethod.POST,
					new HttpEntity<>(sensorObject, headers), myData);
			/*
			 * maybe test here on the responseEntity??
			 * 
			 * do i return anything??
			 */

			// return data;
		} catch (HttpClientErrorException ex) { // all 4xx errors codes
			logger.info("exception", ex);
			// return null;
		}

		// }
	}

}
