package ie.predictiveMaintenance.Controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import ie.predictiveMaintenance.entities.Sensor;
import ie.predictiveMaintenance.services.ProcessDataService;
import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class ProcessController {
	private static Logger logger = LoggerFactory.getLogger(ProcessController.class);
	
	@Autowired
	ProcessDataService processDataService;
	
	public List<Object> getHead(List<Object> data){
		
		List<Object> head = processDataService.head(data);
		return head;
		
	}
	
	public List<Sensor> processData(List<Object> data){
		for (Object dat: data) {
			logger.info("processController pm tool preprocess " + dat.toString());
			
		}
		List<Sensor> dataStatusSet = processDataService.processData(data);
		for (Sensor dataStatus: dataStatusSet) {
			//Sensor sensor = (Sensor)dataStatus;
			logger.info("processController pm tool" + dataStatus.getName() + " " + dataStatus.getData());
			
		}
		return dataStatusSet;
		
	}
	
	public Sensor processDataById(Object data){
		Sensor dataStatusSet = processDataService.processDataById(data);
		logger.info("processController pm tool " + dataStatusSet.getData().toString());
		return dataStatusSet;
		
	}
}
