package ie.predictiveMaintenance.services;

import java.util.List;

import ie.predictiveMaintenance.entities.Sensor;

public interface ProcessDataService {

	List<String> halveData(List<String> data);
	List<Object> head(List<Object> data);
	Sensor processDataById(Object data);
	List<Sensor> processData(List<Object> data);
}
