package ie.predictiveMaintenance.services;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ie.predictiveMaintenance.entities.Sensor;

@Service
public class ProcessDataServiceImpl implements ProcessDataService{
	private static Logger logger = LoggerFactory.getLogger(ProcessDataServiceImpl.class);
	
//	@Autowired
//	private Sensor sensor;
	
	@Override
	public List<String> halveData(List<String> data) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object> head(List<Object> data) {
		List<Object> head = new ArrayList<Object>();
		if(data.size() > 0) {
			for(int i = 0;i < 20;i++) {
				head.add(data.get(i));
			}
		}
		else {
			logger.info("no data in kafka");
		}
		return head;
	}

	@Override
	public List<Sensor> processData(List<Object> data) {
		//read through data, convert Object to usable class
		//this just adds a status field and sets to good, for now
		List<Sensor> processedData = new ArrayList<Sensor>();
		LinkedHashMap<String, String> lhm;
		String status = "";
		for(Object reading: data) {
			lhm = (LinkedHashMap<String, String>) reading;
			Sensor sensor = new Sensor();
			sensor.setName(lhm.get("name")  + "_status");
			status = calcStatus(lhm);
			sensor.setStatus(status);
			sensor.setData(lhm.get("data"));
			logger.info("sensor, ProcessDataServiceImpl" + sensor.getName() + " " + sensor.getData());
			processedData.add(sensor);
		}
		for (Sensor dat: processedData) {
			logger.info("processData,  " + dat.toString());
			
		}
		return processedData;
	}
	
	@Override
	public Sensor processDataById(Object data) {
		//this just adds a status field and sets to good, for now		
		LinkedHashMap<String, String> lhm = (LinkedHashMap<String, String>) data;
		Sensor sensor = new Sensor();
		sensor.setName(lhm.get("name")  + "_status");
		sensor.setStatus("good");
		sensor.setData(lhm.get("data"));
		
		return sensor;
	}

	private String calcStatus(LinkedHashMap<String, String> lhm) {
		String status = "";
		String data = lhm.get("data");
		String[] splitted = data.split(" ");
		int humidity = Integer.parseInt(splitted[3]);
		int temp = Integer.parseInt(splitted[5]);
		
		if((humidity < 75 && temp < 15)  || (humidity > 85 && temp > 25))
			status = "bad";
		else status = "good";
		
		return status;
		
	}
}
